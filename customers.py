import utils

class Customers:

    def __init__(self, customers):
        self.update(customers)

    def update(self, customers):
        self.customers = customers
        self._current_groupping_by_car = None

    def get_waiting(self):
        return [
            customer_id
            for customer_id, customer_info in self.customers.items()
            if customer_info['status'] == 'waiting'
        ]

    def get_car_id(self, customer_id):
        return self.customers[customer_id]['car_id']

    def get_origin(self, customer_id):
        return self.customers[customer_id]['origin']

    def get_destination(self, customer_id):
        return self.customers[customer_id]['destination']

    @property
    def groupped_by_car(self):
        if self._current_groupping_by_car is None:
            self._current_groupping_by_car = utils.groupby_expanded(
                self.customers,
                lambda customer_id: self.customers[customer_id]['car_id']
            )
        return self._current_groupping_by_car
