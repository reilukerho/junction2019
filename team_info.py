from dataclasses import dataclass

@dataclass
class TeamInfo:
    team_id: str
    team_name: str
    team_token: str



# testing teams
team_A = TeamInfo(
    team_id=0,
    team_name='team_A',
    team_token='d7300481'
)

team_B = TeamInfo(
    team_id=1,
    team_name='team_B',
    team_token='a6fbce25'
)
