from enum import Enum
import networkx

class CardinalDirections(Enum):
    NORTH = ( 0, 1)
    SOUTH = ( 0,-1)
    EAST  = ( 1, 0)
    WEST  = (-1, 0)

    def to_server(self):
        if self   == CardinalDirections.NORTH: return ServerDirections.NORTH
        elif self == CardinalDirections.SOUTH: return ServerDirections.SOUTH
        elif self == CardinalDirections.EAST:  return ServerDirections.EAST
        elif self == CardinalDirections.WEST:  return ServerDirections.WEST
        else: raise ValueError(f'{self} is a weird direction')

class ServerDirections(Enum):
    NORTH = 0
    SOUTH = 2
    EAST  = 1
    WEST  = 3

    def to_cardinal(self):
        if self == ServerDirections.NORTH: return CardinalDirections.NORTH
        elif self == ServerDirections.SOUTH: return CardinalDirections.SOUTH
        elif self == ServerDirections.EAST: return CardinalDirections.EAST
        elif self == ServerDirections.WEST: return CardinalDirections.WEST
        else: raise ValueError(f'{self} is a weird direction')

ROAD = True
WALL = False

def get_neighbors(grid_sequential, width, index):
    neighbors = []

    assert grid_sequential[index] == ROAD

    X, Y = index_to_coordinates(index, width)
    for direction in CardinalDirections:
        dx, dy = direction.value
        x, y = X+dx, Y+dy
        if (0 <= x < width) and (0 <= y < width):
            new_index = coordinates_to_index(x, y, width)
            if grid_sequential[new_index] == ROAD:
                neighbors.append(new_index)

    return neighbors


def index_to_coordinates(index, width):
    x = index % width
    y = index // width
    return x, y

def coordinates_to_index(x, y, width):
    return x + width * y

def create_graph(grid_sequential, width):
    """
    returns networkx graph
    you can do operations such as networkx.shortest_path(G, source, destination)
    """

    graph = networkx.Graph()

    # graph.add_nodes_from(range(len(grid_sequential)))

    for index, cell in enumerate(grid_sequential):
        if cell == ROAD:
            neighbors = get_neighbors(grid_sequential, width, index)
            edge_generator = (
                (index, neighbor)
                for neighbor in neighbors
            )
            graph.add_edges_from(edge_generator)

    return graph

def edge_to_server_direction(source, target, width):
    return edge_to_cardinal_direction(source, target, width).to_server()

def edge_to_cardinal_direction(source, target, width):

    length = width*width
    assert 0 <= source < length
    assert 0 <= target < length

    Xs, Ys = index_to_coordinates(source, width)
    Xt, Yt = index_to_coordinates(target, width)
    return CardinalDirections((Xt - Xs, Yt - Ys))

def shortest_path_no_fail(graph, source, target):
    try:
        return networkx.shortest_path(graph, source, target)
    except networkx.NetworkXNoPath:
        pass
    except Exception as e:
        print(e)

def shortest_path_length_no_fail(graph, source, target):
    try:
        return networkx.shortest_path_length(graph, source, target)
    except networkx.NetworkXNoPath:
        pass
    except Exception as e:
        print(e)
