from itertools import *
from paths import *

import utils

class Cars:

    def __init__(self, cars, team_id):
        self._team_id = int(team_id)
        self._my_teams_cars = None
        self._other_teams_cars = None
        self.update(cars)

    def update(self, cars):
        self.cars = cars

    def get_position(self, car_id):
        return self.cars[car_id]['position']

    def is_full(self, car_id):
        return self.get_capacity(car_id) == self.get_used_capacity(car_id)

    def get_capacity(self, car_id):
        return self.cars[car_id]['capacity']

    def get_used_capacity(self, car_id):
        return self.cars[car_id]['used_capacity']

    @property
    def my_teams(self):
        if self._my_teams_cars is None:
            self._group_mine_and_other_cars()
        return self._my_teams_cars

    @property
    def other_teams(self):
        if self._other_teams_cars is None:
            self._group_mine_and_other_cars()
        return self._other_teams_cars

    def _group_mine_and_other_cars(self):
        groups = {
            group: tuple(original_keys)
            for group, original_keys in groupby(
                self.cars,
                lambda car_id: int(self.cars[car_id]['team_id']) == self._team_id
            )
        }
        self._my_teams_cars = groups[True]
        self._other_teams_cars = groups[False]
