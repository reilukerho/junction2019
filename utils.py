from itertools import *
from paths import *
import networkx

def filter_dict(dictionary, callback: 'return truthy to keep, falsy to drop'):
    return {
        key: value
        for key, value in dictionary.items()
        if callback(key, value)
    }

def groupby_expanded(iterable, callback):
    return {
        group: tuple(original_keys)
        for group, original_keys in groupby(iterable, callback)
    }



def find_closest_passanger_destination(car_id, cars, customers, graph):
    car_position = cars.get_position(car_id)

    customer_ids = customers.groupped_by_car[car_id]
    destinations = [
        customers.get_destination(customer_id)
        for customer_id in customer_ids
    ]

    return closest_destination(graph, car_position, destinations)

def find_closest_passanger(car_id, cars, customers, graph):
    car_position = cars.get_position(car_id)

    customer_ids = customers.get_waiting()
    destinations = [
        customers.get_destination(customer_id)
        for customer_id in customer_ids
    ]

    return closest_destination(graph, car_position, destinations)

def closest_destination(graph, source, destinations):
    distances = [
        distance
        for destination in destinations
        if (distance := shortest_path_length_no_fail(graph, source, destination))
    ]

    sorted_by_distance = sorted(zip(distances, destinations))
    if not sorted_by_distance: return
    closest_destination = sorted_by_distance[0][1]
    return closest_destination


def go_to_target(car_id, destination, cars, graph, connection, width):
    car_position = cars.get_position(car_id)
    path = shortest_path_no_fail(graph, car_position, destination)
    if not path or len(path) < 2: return
    direction = edge_to_server_direction(car_position, path[1], width)
    connection.move_car(car_id, direction)

