from itertools import *
from team_info import *
from paths import *
from utils import *

from connection_handler import CitySimulation

import world_attributes as wa
from cars import Cars
from customers import Customers
import utils


my_team = TeamInfo(
    team_id=0,
    team_name='team_A',
    team_token='d7300481'
)
cs = CitySimulation(my_team.team_token)

world = cs.get_next_world()
WIDTH = world[wa.WIDTH]
cars = Cars(world[wa.CARS], my_team.team_id)
customers = Customers(world[wa.CUSTOMERS])

print(cars.my_teams)

while True:
    try:
        cars.update(world[wa.CARS])
        customers.update(world[wa.CUSTOMERS])
        graph = create_graph(world[wa.GRID], WIDTH)

        waiting_customers = customers.get_waiting()

        for car_id in cars.my_teams:
            destination = None
            if cars.is_full(car_id):
                destination = find_closest_passanger_destination(car_id, cars, customers, graph)
            else:
                destination = find_closest_passanger(car_id, cars, customers, graph)

            if destination:
                go_to_target(car_id, destination, cars, graph, cs, WIDTH)

        world = cs.get_next_world()

    except Exception as e:
        print(e)
