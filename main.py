from itertools import *
from team_info import *
from paths import *
from utils import *

from connection_handler import CitySimulation

import world_attributes as wa
from cars import Cars
from customers import Customers
import utils


my_team = TeamInfo(
        team_id=4,
        team_name='shannon',
        team_token='34898d6e'
)
cs = CitySimulation(my_team.team_token)

world = cs.get_next_world()
WIDTH = world[wa.WIDTH]
cars = Cars(world[wa.CARS], my_team.team_id)
customers = Customers(world[wa.CUSTOMERS])

print(cars.my_teams)

CD = {}
while True:
        cars.update(world[wa.CARS])
        customers.update(world[wa.CUSTOMERS])
        graph = create_graph(world[wa.GRID], WIDTH)

        waiting_customers = customers.get_waiting()

        for car_id in cars.my_teams:
            try:
                car_position = cars.get_position(car_id)
                print(f'car {car_id} at {car_position}')
                if cars.is_full(car_id):
                    waiting_customers = []
                else:
                    waiting_customers = [customers.get_origin(customer_id) for customer_id in customers.get_waiting()]
                print(f'\t{waiting_customers}')
                print(customers.groupped_by_car)
                customer_destinations = CD.get(car_id, [])
                print(f'\t{customer_destinations}')
                targets = waiting_customers + customer_destinations
                destination = closest_destination(graph, car_position, targets)
                print(f'\theading to {destination}')
                if not destination: continue
                path = shortest_path_no_fail(graph, car_position, destination)
                if not path: continue
                print(f'\t{path}')
                direction = edge_to_server_direction(car_position, path[1], WIDTH)
                print(f'\t{direction}')
                if destination in waiting_customers:
                    for customer_id in customers.get_waiting():
                        if customers.get_origin(customer_id) == destination:
                            CD.setdefault(car_id, []).append(customers.get_destination(customer_id))
                            continue
                cs.move_car(car_id, direction)
            except Exception as e:
                print(e)
            # try:
            #     destination = None
            #     if cars.is_full(car_id):
            #         print(f'car {car_id} is full', end=', ')
            #         destination = find_closest_passanger_destination(car_id, cars, customers, graph)
            #         print(f'heading to {destination}')
            #     else:
            #         destination = find_closest_passanger(car_id, cars, customers, graph)
            #         print(f'car {car_id} is heading to {destination}')

            #     if destination:
            #         go_to_target(car_id, destination, cars, graph, cs, WIDTH)

            # except Exception as e:
            #     print(e)

        world = cs.get_next_world()

