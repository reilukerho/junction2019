import numpy as np

class Grid:

    def __init__(self, world: 'from processed JSON'):
        self.width = world['width']
        self.height = world['height']
        new_shape = [self.width, self.height]
        self.grid = np.asarray(world['grid']).reshape(shape)

    def __getitem__(self, x_or_index, y=None):
        if y:
            return self.grid[x_or_index, y]
        else:
            return self.grid.flat[x_or_index]

