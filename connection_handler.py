import requests
import world_attributes as wa
import json

class CitySimulation:

    # SERVER_URL = 'http://localhost:8080'
    SERVER_URL = 'https://api.citysimulation.eu/lars'
    # SERVER_URL = 'https://api.citysimulation.eu/[team_name]'
    API_BASE_URL = SERVER_URL + '/api/v1'
    WORLD_STATUS_URL = API_BASE_URL + '/world'
    SCORES_URL = API_BASE_URL + '/scores'
    ACTIONS_URL = API_BASE_URL + '/actions'

    def __init__(self, token, timeout=0.5):
        self.token = token
        self.timeout = timeout
        self.previous_world = {wa.TICKS: -1}

    def get_next_world(self):
        previous_ticks = self.previous_world[wa.TICKS]

        while True:
            try:
                latest_world = self._get_latest_world()
                if latest_world[wa.TICKS] != previous_ticks:
                    break
            except Exception as e:
                print(e)

        self.previous_world = latest_world

        return latest_world

    def _get_latest_world(self):
        r = requests.get(self.WORLD_STATUS_URL, timeout=self.timeout)
        return r.json()

    def move_car(self, car_id, direction: 'ServerDirections'):
        request_content = json.dumps({
            'Type': 'move',
            'Action': {
                'CarId': int(car_id),
                'MoveDirection': direction.value
            }
        })
        return requests.post(
            self.ACTIONS_URL,
            request_content,
            headers={'Authorization': self.token},
            timeout=self.timeout
        )

    def get_scores(self):
        raise NotImplementedError()
